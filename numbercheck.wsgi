#! /usr/bin/python3.6

activate_this = '/var/www/flaskapp/numbercheck/venv/bin/activate_this.py'

with open(activate_this) as f:
 exec(f.read(), dict(__file__=activate_this))


import logging
import sys


logging.basicConfig(stream=sys.stderr)
sys.path.insert(0, '/var/www/flaskapp/numbercheck/')
from numbercheck import app as application
application.secret_key = 'i%KSDHJFOJskAJODHDHFkdshfsldfhsd⁾@#*(92837'


