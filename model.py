from datetime import datetime
from numbercheck import db, ma





db = SQLAlchemy(app)
ma = Marshmallow(app)

class Number(db.Model):
    number_id = db.Column(db.Integer, primary_key=True)
    number = db.Column(db.String(20), nullable=False)
    check = db.Column(db.Integer, nullable=False)
    fix = db.Column(db.String(20), nullable=True)
    register_time = db.Column(db.DateTime, default=datetime.now())

class NumberSchema(ma.ModelSchema):
    class Meta:
        model = Number
