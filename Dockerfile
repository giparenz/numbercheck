FROM python:3

ADD numbercheck.py /opt
ADD model.py /opt 
ADD South_African_Mobile_Numbers.csv /opt
WORKDIR /opt
RUN pip install flask
RUN pip install flask_api
RUN pip install flask_sqlalchemy
RUN pip install pipenv
RUN pip install flask_marshmallow
RUN pip install marshmallow_sqlalchemy
CMD ["python", "./numbercheck.py"]
