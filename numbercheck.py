import os
from flask import Flask, jsonify , flash, request, redirect, render_template
from flask import request
from flask_api import status
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from sqlalchemy.exc import SQLAlchemyError
from datetime import datetime
import urllib.request
from werkzeug.utils import secure_filename
import re 

appdir = "/var/www/flaskapp/numbercheck/"

project_dir = os.path.dirname(os.path.abspath(__file__))

database_file = "sqlite:///{}".format(os.path.join(project_dir, "number.db"))

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = database_file
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["DEBUG"] = True

UPLOAD_FOLDER = appdir + 'files/'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024

ALLOWED_EXTENSIONS = set(['txt' , 'csv'])

db = SQLAlchemy(app)
ma = Marshmallow(app)

class Number(db.Model):
    number_id = db.Column(db.Integer, primary_key=True)
    number = db.Column(db.String(20), nullable=False)
    check = db.Column(db.Integer, nullable=False)
    fix = db.Column(db.String(20), nullable=True)
    register_time = db.Column(db.DateTime, default=datetime.now())

class NumberSchema(ma.ModelSchema):
    class Meta:
        model = Number

def allowed_file(filename):
  return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS



def checknum(num):    
    #phoneNumRegex = re.compile(r'27(\d{9})')
    if num.isdigit():
      phoneNumRegex = re.compile(r'^(27)(\d{9})$')
      m = phoneNumRegex.search(num)
      if m :
        return (num,0,"ok")
      else:
        if len(num) == 9 :
          num = "27" + num 
          return (num,1,"fixed adding 27 before number")
        elif num[:4] == '0027' and len(num) == 13 :
          num = num[2:]
          return (num,2,"fixed taking off 00")
        elif num[:1] == '0' and len(num) == 10 :
          num = "27" + num[1:]
          return (num,3,"fixed taking off 0 and adding 27 before number")
        else:  
          return (num,4,"wrong number")
    else:  
      return (num,4,"wrong number")




@app.route('/upload')
def upload_form():
  return render_template('upload.html')
  

  
@app.route('/upload', methods=['POST'])
def upload_file():
  if request.method == 'POST':
        # check if the post request has the file part
    if 'file' not in request.files:
      flash('No file part')
      return redirect(request.url)
    file = request.files['file']
    if file.filename == '':
      flash('No file selected for uploading')
      return redirect(request.url)
    if file and allowed_file(file.filename):
      filename = secure_filename(file.filename)
      file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
      flash('File successfully uploaded')
      return redirect('/upload')
    else:
      flash('Allowed file types are txt, csv')
      return redirect(request.url)


@app.route("/db", methods=["GET"])
def cretedb():
  try:
    db.create_all()
    content = {' DB created '}
    return content, status.HTTP_200_OK  
  except SQLAlchemyError as e:
    reason = str(e)
    content = {'reason': reason}
    return content, status.HTTP_400_BAD_REQUEST

@app.route("/scanfile", methods=["GET"])
def scanfile(): 
      if request.args.get("filename") is None:
         filename = 'South_African_Mobile_Numbers.csv'
      else:
         filename = request.args.get("filename")
      try:
        
        with open( app.config['UPLOAD_FOLDER'] + filename ,'r', encoding='utf-8') as f:
          try:
            for line in f:
              num = str(line.split(",")[1]).strip('\n')
              numid = line.split(",")[0]
              res = checknum(num)
              if numid.isdigit() and not Number.query.filter_by(number_id=numid).first():
              
                  cu = Number(number_id=numid, number=res[0], check=res[1], fix=res[2] )
                  db.session.add(cu)
            db.session.commit()
          except SQLAlchemyError as e:
            reason = str(e)
            content = {'reason': reason}
            return content, status.HTTP_400_BAD_REQUEST
              
               #content = {'response': 'created'}
               #return content, status.HTTP_201_CREATED
      except IOError as e: 
        content = {'reason' : str(e)}
        return content, status.HTTP_406_NOT_ACCEPTABLE
      try:
        all_nums_query = Number.query.all()
        num_schema = NumberSchema(many=True)
        num_list = num_schema.dump(all_nums_query)
        return jsonify({'list': num_list}), status.HTTP_200_OK
      except SQLAlchemyError as e:
        reason = str(e)
        content = {'reason': reason}
        return content, status.HTTP_400_BAD_REQUEST

@app.route("/displayall", methods=["GET"])
def display():
    try:
        all_nums_query = Number.query.all()
        nums_schema = NumberSchema(many=True)
        nums_list = nums_schema.dump(all_nums_query)
        return jsonify({'list': nums_list})
    except SQLAlchemyError as e:
        reason = str(e)
        content = {'reason': reason}
        return content, status.HTTP_400_BAD_REQUEST

@app.route("/nice", methods=["GET"])
def nice():
    try:
        all_nums_query = Number.query.all()
        nums_schema = NumberSchema(many=True)
        content = "<h1> Data in File </h1>"
        content += "<table><tr><td> numero </td><td> fix </td></tr>"
        for line in nums_schema.dump(all_nums_query):
            if line['check'] == 4 : 
              content += "<tr><td><font color=\"red\">"+str(line['number'])+"</font></td><td>"+str(line['fix'])+"</tr></td>"
            else:
              content += "<tr><td>"+str(line['number'])+"</td><td>"+str(line['fix'])+"</tr></td>"
        content += "</table>"
        return content, status.HTTP_200_OK
    except SQLAlchemyError as e:
        reason = str(e)
        content = {'reason': reason}
        return content, status.HTTP_400_BAD_REQUEST
@app.route("/filter", methods=["GET"])
def filter():
    try:
        all_nums_query = Number.query.filter_by(check=request.args.get("checkid"))
        nums_schema = NumberSchema(many=True)
        content = "<h1> Data in File </h1>"
        content += "<table><tr><td> numero </td><td> fix </td></tr>"
        for line in nums_schema.dump(all_nums_query):
            if line['check'] == 4 : 
              content += "<tr><td><font color=\"red\">"+str(line['number'])+"</font></td><td>"+str(line['fix'])+"</tr></td>"
            else:
              content += "<tr><td>"+str(line['number'])+"</td><td>"+str(line['fix'])+"</tr></td>"
        content += "</table>"
        return content, status.HTTP_200_OK
    except SQLAlchemyError as e:
        reason = str(e)
        content = {'reason': reason}
        return content, status.HTTP_400_BAD_REQUEST
@app.route("/check", methods=["GET"])
def check():
        if request.args.get("number") is None:
            content = {'response': ' number is missed '}
            return content, status.HTTP_406_NOT_ACCEPTABLE
        else:
            res = checknum(request.args.get("number"))
            content = "<h1> check single numer  </h1><p>"+str(res[0])+"<br>"+str(res[1])+"<br>"+str(res[2])+"</p>"
            return content, status.HTTP_200_OK


@app.errorhandler(404)
def page_not_found(e):
    return "<h1>404</h1><p>The resource could not be found.</p>", 404


def main():
    db.create_all()
    app.run(debug=True, host='0.0.0.0')

if __name__ == "__main__":
    main()
