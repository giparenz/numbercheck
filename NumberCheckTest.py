import unittest
import requests


class MyTestCase(unittest.TestCase):

    def test_scanfile(self):
        resp = requests.get('http://127.0.0.1:5000/scanfile')
        self.assertEqual(resp.status_code, 200)
    
    def test_displayall(self):
        resp = requests.get('http://127.0.0.1:5000/displayall')
        self.assertEqual(resp.status_code, 200)
    
    def test_nice(self):
        resp = requests.get('http://127.0.0.1:5000/nice')
        self.assertEqual(resp.status_code, 200)

    def test_filter(self):
    	for i in [0,1,2,3,4]:
          resp = requests.get('http://127.0.0.1:5000/filter?checkid='+str(i))
          self.assertEqual(resp.status_code, 200)
    
    def test_number(self):
    	for i in ['27821234567','0731234567','0027841234567']:
          resp = requests.get('http://127.0.0.1:5000/check?number='+i)
          self.assertEqual(resp.status_code, 200) 



if __name__ == '__main__':
    unittest.main()
